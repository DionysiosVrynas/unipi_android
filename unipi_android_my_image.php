 <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "unipi_android";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$query = mysqli_query("select * from photos");
$response = array();
while($row = mysqli_fetch_assoc($query))
{
$response[] = $row['photo_url'];
}
echo json_encode($response);
mysqli_close($con);
?>
