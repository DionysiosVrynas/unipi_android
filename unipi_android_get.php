 <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "unipi_android";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

	$query = " select * from highscores order by highscore desc LIMIT 10;";
	$result = mysqli_query($conn, $query);
	$number_of_rows = mysqli_num_rows($result);
	$temp_array = array();
	if($number_of_rows > 0 ){
		while ($row = mysqli_fetch_assoc($result)){
			$temp_array[] = $row;
		}
	}
	header('Content-Type: application/json');
	echo json_encode(array("highscores"=>$temp_array));
	mysqli_close($conn);

?>