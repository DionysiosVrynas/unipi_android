package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example_calculator2.dennis.unipi_android.model.Student;
import com.example_calculator2.dennis.unipi_android.service.APIService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class Registration extends AppCompatActivity {

    EditText etid;
    EditText etname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        etid = (EditText)findViewById(R.id.editText);
        etname = (EditText)findViewById(R.id.editText2);
        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorRegistration);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registration.this,Welcome.class);
                startActivity(intent);
                Registration.this.finish();
            }
        });
    }

    public void Register(View view) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://83.212.102.242/unipi_android_register.php")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Student student = new Student();
        student.setPassword(etname.getText().toString());
        student.setId(etid.getText().toString());
        Call<Student> call = service.Register(student.getId(),student.getPassword());
        call.enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Response<Student> response, Retrofit retrofit) {
                Log.d("onResponse", "" + response.code() +
                        "response body " + response.body() + "response error " + response.errorBody() + " responseMessage " + response.message());
              }

            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl("http://83.212.102.242/unipi_android_post.php")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service1 = retrofit1.create(APIService.class);
        Student student1 = new Student();
        student1.setName(etname.getText().toString());
        student1.setId(etid.getText().toString());
        Call<Student> call1 = service1.insertStudentInfo(student1.getId(),student1.getName(),"0");
        call1.enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Response<Student> response, Retrofit retrofit) {
                Log.d("onResponse", "" + response.code() +
                        "response body " + response.body() + "response error " + response.errorBody() + " responseMessage " + response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
