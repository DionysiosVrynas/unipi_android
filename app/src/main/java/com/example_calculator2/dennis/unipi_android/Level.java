package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Level extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        Button btnEASY = (Button)findViewById(R.id.btnEASY);
        Button btnNORMAL = (Button)findViewById(R.id.btnNORMAL);
        Button btnHARD = (Button)findViewById(R.id.btnHARD);
        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorLevel);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level.this, Snake.class);
                startActivity(intent);
                Level.this.finish();
            }
        });

        btnEASY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level.this, Game_1_vs_Com_EASY.class);
                startActivity(intent);
                Level.this.finish();
            }
        });

        btnNORMAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level.this, Game_1_vs_Com_Normal.class);
                startActivity(intent);
                Level.this.finish();
            }
        });

        btnHARD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level.this, Game_1_vs_Com_HARD.class);
                startActivity(intent);
                Level.this.finish();
            }
        });
    }
}
