package com.example_calculator2.dennis.unipi_android;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

class PlayerAdapter extends ArrayAdapter{

    private List list = new ArrayList();
    PlayerAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row;
        row = convertView;
        PlayerHolder playerHolder;
        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.activity_row_layout,parent,false);
            playerHolder = new PlayerHolder();
            playerHolder.column1 = (TextView)row.findViewById(R.id.column1);
            playerHolder.column2 = (TextView)row.findViewById(R.id.column2);
            playerHolder.column3 = (TextView)row.findViewById(R.id.column3);
            row.setTag(playerHolder);
        }else {
            playerHolder = (PlayerHolder)row.getTag();
        }
        Players players = (Players) this.getItem(position);
        assert players != null;
        playerHolder.column1.setText((players.getId()));
        playerHolder.column2.setText((players.getName()));
        playerHolder.column3.setText((players.getHighscore()));
        return row;
    }

    private static class PlayerHolder{
        TextView column1,column2,column3;
    }
}
