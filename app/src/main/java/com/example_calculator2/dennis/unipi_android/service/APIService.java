package com.example_calculator2.dennis.unipi_android.service;


import com.example_calculator2.dennis.unipi_android.model.Student;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;


public interface APIService {

    @FormUrlEncoded
    @POST("http://83.212.102.242/unipi_android_post.php")
    Call<Student> insertStudentInfo(@Field("id") String id, @Field("name") String name, @Field("highscore") String highscore);


    @FormUrlEncoded
    @POST("http://83.212.102.242/unipi_android_register.php")
    Call<Student> Register(@Field("id") String id, @Field("password") String password);

    @FormUrlEncoded
    @POST("http://83.212.102.242/unipi_android_put.php")
    Call<Student> Update(@Field("id") String id, @Field("name") String name, @Field("highscore") String highscore);

    @FormUrlEncoded
    @POST("http://83.212.102.242/unipi_android_image_post.php")
    Call<Student> insertimage(@Field("id") String id, @Field("name") String name, @Field("image") String image);
}
