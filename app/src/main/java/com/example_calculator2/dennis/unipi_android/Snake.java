package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Snake extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snake);

        Button btn1vsCOM = (Button)findViewById(R.id.btn1vsCOM);
        Button btnMULTIPLAYER = (Button)findViewById(R.id.btnMULTIPLAYER);
        Button btnSETTINGS = (Button)findViewById(R.id.btnSETTINGS);
        Button btnHIGHSCORE = (Button)findViewById(R.id.btnHIGHSCORE);
        Button btnEXIT = (Button)findViewById(R.id.btnEXIT);
        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorSnake);

        btnHIGHSCORE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Snake.this, Highscore.class);
                startActivity(intent);
                Snake.this.finish();
            }
        });

        btn1vsCOM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Snake.this, Level.class);
                startActivity(intent);
                Snake.this.finish();
            }
        });

        btnMULTIPLAYER.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Snake.this, Level_Multiplayer.class);
                startActivity(intent);
                Snake.this.finish();
            }
        });

        btnSETTINGS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Snake.this, Settings.class);
                startActivity(intent);
                Snake.this.finish();
            }
        });

        btnEXIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snake.this.finish();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Snake.this, Welcome.class);
                startActivity(intent);
                Snake.this.finish();
            }
        });
    }
}
