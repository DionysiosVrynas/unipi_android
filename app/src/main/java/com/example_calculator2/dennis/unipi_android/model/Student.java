package com.example_calculator2.dennis.unipi_android.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Student {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("highscore")
    @Expose
    private String highscore;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("password")
    @Expose
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHighscore() {
        return highscore;
    }

    public void setHighscore(String highscore) {
        this.highscore = highscore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}