package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Level_Multiplayer extends AppCompatActivity {

    private  Button easy,normal,hard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level__multiplayer);

        easy = (Button)findViewById(R.id.btnEASY_multi);
        normal = (Button)findViewById(R.id.btnNORMAL_multi);
        hard = (Button)findViewById(R.id.btnHARD_multi);
        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorLevel2);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level_Multiplayer.this, Snake.class);
                startActivity(intent);
                Level_Multiplayer.this.finish();
            }
        });

        easy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level_Multiplayer.this, Game_Multi_EASY.class);
                startActivity(intent);
                Level_Multiplayer.this.finish();
            }
        });

        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level_Multiplayer.this, Game_Multi_NORMAL.class);
                startActivity(intent);
                Level_Multiplayer.this.finish();
            }
        });

        hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Level_Multiplayer.this, Game_Multi_HARD.class);
                startActivity(intent);
                Level_Multiplayer.this.finish();
            }
        });
    }
}
