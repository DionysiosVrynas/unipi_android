package com.example_calculator2.dennis.unipi_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example_calculator2.dennis.unipi_android.model.Student;
import com.example_calculator2.dennis.unipi_android.service.APIService;

import java.util.Random;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class Game_1_vs_Com_Normal extends AppCompatActivity {

    private String color, color2, d, iduser;
    private Random r;
    private int turn = 0;
    private Button roll;
    private TextView tx1, tx2, tx3, tx4, tx5, tx6, tx7, tx8, tx9, tx10, tx11, tx12, tx13, tx14, tx15, tx16, tx17, tx18, tx19, tx20, tx21, tx22, tx23, tx24, tx25, tx26, tx27, tx28, tx29, tx30, tx31, tx32, tx33, tx34, tx35, tx36, tx37, tx38;
    private TextView tx39, tx40, tx41, tx42, tx43, tx44, tx45, tx46, tx47, tx48, tx49, tx50, turns, points1, points2, name1, com;
    private Double pts1, pts2;
    private Boolean firsttime1 = true;
    private Boolean firsttime2 = true;
    private Boolean winner;
    private ProgressDialog pDialog;
    private ImageView img, img1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_1_vs__com__normal);

        SharedPreferences sp1 = getSharedPreferences("user_SettingsColor", Context.MODE_PRIVATE);
        SharedPreferences sp2 = getSharedPreferences("user_id", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sp1.edit();
        SharedPreferences.Editor editor2 = sp2.edit();
        iduser = sp2.getString("user_id", iduser);
        editor2.apply();
        color = sp1.getString("color", color);
        editor1.apply();
        color2 = sp1.getString("color2", color2);
        editor1.apply();
        pts1 = 0.0;
        pts2 = 0.0;
        winner = false;
        img = (ImageView) findViewById(R.id.imageViewplayer);
        img1 = (ImageView) findViewById(R.id.imageViewcom);
        roll = (Button) findViewById(R.id.button6);
        points1 = (TextView) findViewById(R.id.textView7);
        points2 = (TextView) findViewById(R.id.textView8);
        turns = (TextView) findViewById(R.id.textView4);
        name1 = (TextView) findViewById(R.id.textView5);
        name1.setText(sp1.getString("editText", d));
        editor1.apply();
        com = (TextView) findViewById(R.id.textView6);
        tx1 = (TextView) findViewById(R.id.textView59);
        tx2 = (TextView) findViewById(R.id.textView58);
        tx3 = (TextView) findViewById(R.id.textView57);
        tx4 = (TextView) findViewById(R.id.textView56);
        tx5 = (TextView) findViewById(R.id.textView55);
        tx6 = (TextView) findViewById(R.id.textView54);
        tx7 = (TextView) findViewById(R.id.textView53);
        tx8 = (TextView) findViewById(R.id.textView52);
        tx9 = (TextView) findViewById(R.id.textView51);
        tx10 = (TextView) findViewById(R.id.textView50);
        tx11 = (TextView) findViewById(R.id.textView49);
        tx12 = (TextView) findViewById(R.id.textView48);
        tx13 = (TextView) findViewById(R.id.textView47);
        tx14 = (TextView) findViewById(R.id.textView46);
        tx15 = (TextView) findViewById(R.id.textView45);
        tx16 = (TextView) findViewById(R.id.textView44);
        tx17 = (TextView) findViewById(R.id.textView43);
        tx18 = (TextView) findViewById(R.id.textView42);
        tx19 = (TextView) findViewById(R.id.textView41);
        tx20 = (TextView) findViewById(R.id.textView40);
        tx21 = (TextView) findViewById(R.id.textView39);
        tx22 = (TextView) findViewById(R.id.textView38);
        tx23 = (TextView) findViewById(R.id.textView37);
        tx24 = (TextView) findViewById(R.id.textView36);
        tx25 = (TextView) findViewById(R.id.textView35);
        tx26 = (TextView) findViewById(R.id.textView34);
        tx27 = (TextView) findViewById(R.id.textView33);
        tx28 = (TextView) findViewById(R.id.textView32);
        tx29 = (TextView) findViewById(R.id.textView31);
        tx30 = (TextView) findViewById(R.id.textView30);
        tx31 = (TextView) findViewById(R.id.textView29);
        tx32 = (TextView) findViewById(R.id.textView28);
        tx33 = (TextView) findViewById(R.id.textView27);
        tx34 = (TextView) findViewById(R.id.textView26);
        tx35 = (TextView) findViewById(R.id.textView25);
        tx36 = (TextView) findViewById(R.id.textView24);
        tx37 = (TextView) findViewById(R.id.textView23);
        tx38 = (TextView) findViewById(R.id.textView22);
        tx39 = (TextView) findViewById(R.id.textView21);
        tx40 = (TextView) findViewById(R.id.textView20);
        tx41 = (TextView) findViewById(R.id.textView11);
        tx42 = (TextView) findViewById(R.id.textView12);
        tx43 = (TextView) findViewById(R.id.textView13);
        tx44 = (TextView) findViewById(R.id.textView14);
        tx45 = (TextView) findViewById(R.id.textView15);
        tx46 = (TextView) findViewById(R.id.textView16);
        tx47 = (TextView) findViewById(R.id.textView17);
        tx48 = (TextView) findViewById(R.id.textView60);
        tx49 = (TextView) findViewById(R.id.textView18);
        tx50 = (TextView) findViewById(R.id.textView19);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait....");
        pDialog.setCancelable(false);

        switch (color) {
            case "Red":
                img.setImageResource(R.drawable.redpiece);
                break;
            case "Blue":
                img.setImageResource(R.drawable.bluepiece);
                break;
            case "Green":
                img.setImageResource(R.drawable.greenpiece);
                break;
            case "Yellow":
                img.setImageResource(R.drawable.yellowpiece);
                break;
            default:
                img.setImageResource(R.drawable.redpiece);
                break;
        }

        switch (color2) {
            case "Red":
                img1.setImageResource(R.drawable.redpiece);
                break;
            case "Blue":
                img1.setImageResource(R.drawable.bluepiece);
                break;
            case "Green":
                img1.setImageResource(R.drawable.greenpiece);
                break;
            case "Yellow":
                img1.setImageResource(R.drawable.yellowpiece);
                break;
            default:
                img1.setImageResource(R.drawable.redpiece);
                break;
        }

        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorNormal);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game_1_vs_Com_Normal.this, Level.class);
                startActivity(intent);
                Game_1_vs_Com_Normal.this.finish();
            }
        });
    }

    public void onButtonCilckRoll(View v) {
        if (turn % 2 == 0) {
            roll.setEnabled(false);
            FirstPlayer();
        }
        if (turn % 2 == 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    PlayerCom();
                    roll.setEnabled(true);
                }
            }, 1000);
        }
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void FirstPlayer() {
        r = new Random();
        int rolledNumber = r.nextInt(6) + 1;
        ImageView dice = (ImageView) findViewById(R.id.imageView);
        if (firsttime1) {
            if (rolledNumber == 1) {
                dice.setImageResource(R.drawable.dice1);
                img.setX(tx1.getX());
                img.setY(tx1.getY());
            } else if (rolledNumber == 2) {
                dice.setImageResource(R.drawable.dice2);
                img.setX(tx2.getX());
                img.setY(tx2.getY());
            } else if (rolledNumber == 3) {
                dice.setImageResource(R.drawable.dice3);
                img.setX(tx3.getX());
                img.setY(tx3.getY());
            } else if (rolledNumber == 4) {
                dice.setImageResource(R.drawable.dice4);
                img.setX(tx4.getX());
                img.setY(tx4.getY());
                Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        img.setX(tx23.getX());
                        img.setY(tx23.getY());
                    }
                }, 1000);
                rolledNumber = rolledNumber / 2;
            } else if (rolledNumber == 5) {
                dice.setImageResource(R.drawable.dice5);
                img.setX(tx5.getX());
                img.setY(tx5.getY());
            } else if (rolledNumber == 6) {
                dice.setImageResource(R.drawable.dice6);
                img.setX(tx6.getX());
                img.setY(tx6.getY());
            }
            firsttime1 = false;
            pts1 = pts1 + rolledNumber;
            points1.setText(String.valueOf(pts1));
            turns.setText(name1.getText());
            turn++;
        } else {
            if (img.getX() == tx1.getX() && img.getY() == tx1.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx2.getX());
                    img.setY(tx2.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx3.getX());
                    img.setY(tx3.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx4.getX());
                    img.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx23.getX());
                            img.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx5.getX());
                    img.setY(tx5.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx6.getX());
                    img.setY(tx6.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx7.getX());
                    img.setY(tx7.getY());
                }
            } else if (img.getX() == tx2.getX() && img.getY() == tx2.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx3.getX());
                    img.setY(tx3.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx4.getX());
                    img.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx23.getX());
                            img.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx5.getX());
                    img.setY(tx5.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx6.getX());
                    img.setY(tx6.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx7.getX());
                    img.setY(tx7.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx8.getX());
                    img.setY(tx8.getY());
                }
            } else if (img.getX() == tx3.getX() && img.getY() == tx3.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx4.getX());
                    img.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx23.getX());
                            img.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx5.getX());
                    img.setY(tx5.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx6.getX());
                    img.setY(tx6.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx7.getX());
                    img.setY(tx7.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx8.getX());
                    img.setY(tx8.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx9.getX());
                    img.setY(tx9.getY());
                }
            } else if (img.getX() == tx5.getX() && img.getY() == tx5.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx6.getX());
                    img.setY(tx6.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx7.getX());
                    img.setY(tx7.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx8.getX());
                    img.setY(tx8.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx9.getX());
                    img.setY(tx9.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx10.getX());
                    img.setY(tx10.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                }
            } else if (img.getX() == tx6.getX() && img.getY() == tx6.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx7.getX());
                    img.setY(tx7.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx8.getX());
                    img.setY(tx8.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx9.getX());
                    img.setY(tx9.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx10.getX());
                    img.setY(tx10.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                }
            } else if (img.getX() == tx7.getX() && img.getY() == tx7.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx8.getX());
                    img.setY(tx8.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx9.getX());
                    img.setY(tx9.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx10.getX());
                    img.setY(tx10.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                }
            } else if (img.getX() == tx8.getX() && img.getY() == tx8.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx9.getX());
                    img.setY(tx9.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx10.getX());
                    img.setY(tx10.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx14.getX());
                    img.setY(tx14.getY());
                }
            } else if (img.getX() == tx9.getX() && img.getY() == tx9.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx10.getX());
                    img.setY(tx10.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx14.getX());
                    img.setY(tx14.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx15.getX());
                    img.setY(tx15.getY());
                }
            } else if (img.getX() == tx10.getX() && img.getY() == tx10.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx11.getX());
                    img.setY(tx11.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx14.getX());
                    img.setY(tx14.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx15.getX());
                    img.setY(tx15.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx16.getX());
                    img.setY(tx16.getY());
                }
            } else if (img.getX() == tx11.getX() && img.getY() == tx11.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx12.getX());
                    img.setY(tx12.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx14.getX());
                    img.setY(tx14.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx15.getX());
                    img.setY(tx15.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx16.getX());
                    img.setY(tx16.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx17.getX());
                    img.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                }
            } else if (img.getX() == tx12.getX() && img.getY() == tx12.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx13.getX());
                    img.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx28.getX());
                            img.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx14.getX());
                    img.setY(tx14.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx15.getX());
                    img.setY(tx15.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx16.getX());
                    img.setY(tx16.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx17.getX());
                    img.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx18.getX());
                    img.setY(tx18.getY());
                }
            } else if (img.getX() == tx14.getX() && img.getY() == tx14.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx15.getX());
                    img.setY(tx15.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx16.getX());
                    img.setY(tx16.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx17.getX());
                    img.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx18.getX());
                    img.setY(tx18.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx19.getX());
                    img.setY(tx19.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                }
            } else if (img.getX() == tx15.getX() && img.getY() == tx15.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx16.getX());
                    img.setY(tx16.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx17.getX());
                    img.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx18.getX());
                    img.setY(tx18.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx19.getX());
                    img.setY(tx19.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                }
            } else if (img.getX() == tx16.getX() && img.getY() == tx16.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx17.getX());
                    img.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx18.getX());
                    img.setY(tx18.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx19.getX());
                    img.setY(tx19.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                }
            } else if (img.getX() == tx17.getX() && img.getY() == tx17.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx18.getX());
                    img.setY(tx18.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx19.getX());
                    img.setY(tx19.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                }
            } else if (img.getX() == tx18.getX() && img.getY() == tx18.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx19.getX());
                    img.setY(tx19.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                }
            } else if (img.getX() == tx19.getX() && img.getY() == tx19.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx20.getX());
                    img.setY(tx20.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                }
            } else if (img.getX() == tx20.getX() && img.getY() == tx20.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx21.getX());
                    img.setY(tx21.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                }
            } else if (img.getX() == tx21.getX() && img.getY() == tx21.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx22.getX());
                    img.setY(tx22.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx27.getX());
                    img.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx6.getX());
                            img.setY(tx6.getY());
                        }
                    }, 1000);
                }
            } else if (img.getX() == tx22.getX() && img.getY() == tx22.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx23.getX());
                    img.setY(tx23.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx27.getX());
                    img.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx6.getX());
                            img.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx28.getX());
                    img.setY(tx28.getY());
                }
            } else if (img.getX() == tx23.getX() && img.getY() == tx23.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx24.getX());
                    img.setY(tx24.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx27.getX());
                    img.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx6.getX());
                            img.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx28.getX());
                    img.setY(tx28.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx29.getX());
                    img.setY(tx29.getY());
                }
            } else if (img.getX() == tx24.getX() && img.getY() == tx24.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx25.getX());
                    img.setY(tx25.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx27.getX());
                    img.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx6.getX());
                            img.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx28.getX());
                    img.setY(tx28.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx29.getX());
                    img.setY(tx29.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx30.getX());
                    img.setY(tx30.getY());
                }
            } else if (img.getX() == tx25.getX() && img.getY() == tx25.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx26.getX());
                    img.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx45.getX());
                            img.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx27.getX());
                    img.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx6.getX());
                            img.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx28.getX());
                    img.setY(tx28.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx29.getX());
                    img.setY(tx29.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx30.getX());
                    img.setY(tx30.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx31.getX());
                    img.setY(tx31.getY());
                }
            } else if (img.getX() == tx28.getX() && img.getY() == tx28.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx29.getX());
                    img.setY(tx29.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx30.getX());
                    img.setY(tx30.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx31.getX());
                    img.setY(tx31.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx32.getX());
                    img.setY(tx32.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx33.getX());
                    img.setY(tx33.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                }
            } else if (img.getX() == tx29.getX() && img.getY() == tx29.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx30.getX());
                    img.setY(tx30.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx31.getX());
                    img.setY(tx31.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx32.getX());
                    img.setY(tx32.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx33.getX());
                    img.setY(tx33.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                }
            } else if (img.getX() == tx30.getX() && img.getY() == tx30.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx31.getX());
                    img.setY(tx31.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx32.getX());
                    img.setY(tx32.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx33.getX());
                    img.setY(tx33.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                }
            } else if (img.getX() == tx31.getX() && img.getY() == tx31.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx32.getX());
                    img.setY(tx32.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx33.getX());
                    img.setY(tx33.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                }
            } else if (img.getX() == tx32.getX() && img.getY() == tx32.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx33.getX());
                    img.setY(tx33.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                }
            } else if (img.getX() == tx33.getX() && img.getY() == tx33.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx34.getX());
                    img.setY(tx34.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                }
            } else if (img.getX() == tx34.getX() && img.getY() == tx34.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx35.getX());
                    img.setY(tx35.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                }
            } else if (img.getX() == tx35.getX() && img.getY() == tx35.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx36.getX());
                    img.setY(tx36.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                }
            } else if (img.getX() == tx36.getX() && img.getY() == tx36.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx37.getX());
                    img.setY(tx37.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx42.getX());
                    img.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                }
            } else if (img.getX() == tx37.getX() && img.getY() == tx37.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx38.getX());
                    img.setY(tx38.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx42.getX());
                    img.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx43.getX());
                    img.setY(tx43.getY());
                }
            } else if (img.getX() == tx38.getX() && img.getY() == tx38.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx39.getX());
                    img.setY(tx39.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx42.getX());
                    img.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx43.getX());
                    img.setY(tx43.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx44.getX());
                    img.setY(tx44.getY());
                }
            } else if (img.getX() == tx39.getX() && img.getY() == tx39.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx40.getX());
                    img.setY(tx40.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx42.getX());
                    img.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx43.getX());
                    img.setY(tx43.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx44.getX());
                    img.setY(tx44.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                }
            } else if (img.getX() == tx40.getX() && img.getY() == tx40.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx41.getX());
                    img.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx22.getX());
                            img.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx42.getX());
                    img.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx43.getX());
                    img.setY(tx43.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx44.getX());
                    img.setY(tx44.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                }
            } else if (img.getX() == tx42.getX() && img.getY() == tx42.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx43.getX());
                    img.setY(tx43.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx44.getX());
                    img.setY(tx44.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                }
            } else if (img.getX() == tx43.getX() && img.getY() == tx43.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx44.getX());
                    img.setY(tx44.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                }
            } else if (img.getX() == tx44.getX() && img.getY() == tx44.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx50.getX());
                    img.setY(tx50.getY());
                    winner = true;
                }
            } else if (img.getX() == tx45.getX() && img.getY() == tx45.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx50.getX());
                    img.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx45.getX());
                    img.setY(tx45.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL!!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img.getX() == tx46.getX() && img.getY() == tx46.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx50.getX());
                    img.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL!!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx46.getX());
                    img.setY(tx46.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL!!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img.getX() == tx47.getX() && img.getY() == tx47.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx50.getX());
                    img.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL!!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL!!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx47.getX());
                    img.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img.getX() == tx48.getX() && img.getY() == tx48.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img.setX(tx49.getX());
                    img.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img.setX(tx11.getX());
                            img.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img.setX(tx50.getX());
                    img.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img.setX(tx48.getX());
                    img.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            }
            pts1 = pts1 + rolledNumber;
            if (winner) {
                roll.setEnabled(false);
                turns.setText(name1.getText());
                showpDialog();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://83.212.102.242/unipi_android_put.php")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                APIService service = retrofit.create(APIService.class);
                Student student = new Student();
                student.setName(name1.getText().toString());
                student.setId(iduser);
                student.setHighscore(points1.getText().toString());
                Call<Student> call = service.Update(student.getId(), student.getName(), student.getHighscore());
                call.enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Response<Student> response, Retrofit retrofit) {
                        hidepDialog();
                        Log.d("onResponse", "" + response.code() +
                                "response body " + response.body() + "response error " + response.errorBody() + " responseMessage " + response.message());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        hidepDialog();
                        Log.d("onFailure", t.toString());
                    }
                });

                Intent intent = new Intent(Game_1_vs_Com_Normal.this, Winner.class);
                intent.putExtra("winnre's_name", name1.getText());
                intent.putExtra("game", "Game_1_vs_Com_Normal");
                intent.putExtra("winnersID", iduser);
                startActivity(intent);
                Game_1_vs_Com_Normal.this.finish();
            } else {
                turns.setText(name1.getText());
                turn++;
            }
            points1.setText(String.valueOf(pts1));
        }
    }

    private void PlayerCom() {
        r = new Random();
        int rolledNumber = r.nextInt(6) + 1;
        ImageView dice = (ImageView) findViewById(R.id.imageView);
        if (firsttime2) {
            if (rolledNumber == 1) {
                dice.setImageResource(R.drawable.dice1);
                img1.setX(tx1.getX());
                img1.setY(tx1.getY());
            } else if (rolledNumber == 2) {
                dice.setImageResource(R.drawable.dice2);
                img1.setX(tx2.getX());
                img1.setY(tx2.getY());
            } else if (rolledNumber == 3) {
                dice.setImageResource(R.drawable.dice3);
                img1.setX(tx3.getX());
                img1.setY(tx3.getY());
            } else if (rolledNumber == 4) {
                dice.setImageResource(R.drawable.dice4);
                img1.setX(tx4.getX());
                img1.setY(tx4.getY());
                Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        img1.setX(tx23.getX());
                        img1.setY(tx23.getY());
                    }
                }, 1000);
                rolledNumber = rolledNumber / 2;
            } else if (rolledNumber == 5) {
                dice.setImageResource(R.drawable.dice5);
                img1.setX(tx5.getX());
                img1.setY(tx5.getY());
            } else if (rolledNumber == 6) {
                dice.setImageResource(R.drawable.dice6);
                img1.setX(tx6.getX());
                img1.setY(tx6.getY());
            }
            firsttime2 = false;
            turns.setText(com.getText());
            pts2 = pts2 + rolledNumber;
            points2.setText(String.valueOf(pts2));
            turn++;
        } else {
            if (img1.getX() == tx1.getX() && img1.getY() == tx1.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx2.getX());
                    img1.setY(tx2.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx3.getX());
                    img1.setY(tx3.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx4.getX());
                    img1.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx23.getX());
                            img1.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx5.getX());
                    img1.setY(tx5.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx6.getX());
                    img1.setY(tx6.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx7.getX());
                    img1.setY(tx7.getY());
                }
            } else if (img1.getX() == tx2.getX() && img1.getY() == tx2.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx3.getX());
                    img1.setY(tx3.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx4.getX());
                    img1.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx23.getX());
                            img1.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx5.getX());
                    img1.setY(tx5.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx6.getX());
                    img1.setY(tx6.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx7.getX());
                    img1.setY(tx7.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx8.getX());
                    img1.setY(tx8.getY());
                }
            } else if (img1.getX() == tx3.getX() && img1.getY() == tx3.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx4.getX());
                    img1.setY(tx4.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx23.getX());
                            img1.setY(tx23.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx5.getX());
                    img1.setY(tx5.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx6.getX());
                    img1.setY(tx6.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx7.getX());
                    img1.setY(tx7.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx8.getX());
                    img1.setY(tx8.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx9.getX());
                    img1.setY(tx9.getY());
                }
            } else if (img1.getX() == tx5.getX() && img1.getY() == tx5.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx6.getX());
                    img1.setY(tx6.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx7.getX());
                    img1.setY(tx7.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx8.getX());
                    img1.setY(tx8.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx9.getX());
                    img1.setY(tx9.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx10.getX());
                    img1.setY(tx10.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                }
            } else if (img1.getX() == tx6.getX() && img1.getY() == tx6.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx7.getX());
                    img1.setY(tx7.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx8.getX());
                    img1.setY(tx8.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx9.getX());
                    img1.setY(tx9.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx10.getX());
                    img1.setY(tx10.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                }
            } else if (img1.getX() == tx7.getX() && img1.getY() == tx7.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx8.getX());
                    img1.setY(tx8.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx9.getX());
                    img1.setY(tx9.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx10.getX());
                    img1.setY(tx10.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                }
            } else if (img1.getX() == tx8.getX() && img1.getY() == tx8.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx9.getX());
                    img1.setY(tx9.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx10.getX());
                    img1.setY(tx10.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx14.getX());
                    img1.setY(tx14.getY());
                }
            } else if (img1.getX() == tx9.getX() && img1.getY() == tx9.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx10.getX());
                    img1.setY(tx10.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx14.getX());
                    img1.setY(tx14.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx15.getX());
                    img1.setY(tx15.getY());
                }
            } else if (img1.getX() == tx10.getX() && img1.getY() == tx10.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx11.getX());
                    img1.setY(tx11.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx14.getX());
                    img1.setY(tx14.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx15.getX());
                    img1.setY(tx15.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx16.getX());
                    img1.setY(tx16.getY());
                }
            } else if (img1.getX() == tx11.getX() && img1.getY() == tx11.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx12.getX());
                    img1.setY(tx12.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx14.getX());
                    img1.setY(tx14.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx15.getX());
                    img1.setY(tx15.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx16.getX());
                    img1.setY(tx16.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx17.getX());
                    img1.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                }
            } else if (img1.getX() == tx12.getX() && img1.getY() == tx12.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx13.getX());
                    img1.setY(tx13.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx28.getX());
                            img1.setY(tx28.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx14.getX());
                    img1.setY(tx14.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx15.getX());
                    img1.setY(tx15.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx16.getX());
                    img1.setY(tx16.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx17.getX());
                    img1.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx18.getX());
                    img1.setY(tx18.getY());
                }
            } else if (img1.getX() == tx14.getX() && img1.getY() == tx14.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx15.getX());
                    img1.setY(tx15.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx16.getX());
                    img1.setY(tx16.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx17.getX());
                    img1.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx18.getX());
                    img1.setY(tx18.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx19.getX());
                    img1.setY(tx19.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                }
            } else if (img1.getX() == tx15.getX() && img1.getY() == tx15.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx16.getX());
                    img1.setY(tx16.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx17.getX());
                    img1.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx18.getX());
                    img1.setY(tx18.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx19.getX());
                    img1.setY(tx19.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                }
            } else if (img1.getX() == tx16.getX() && img1.getY() == tx16.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx17.getX());
                    img1.setY(tx17.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx18.getX());
                    img1.setY(tx18.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx19.getX());
                    img1.setY(tx19.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                }
            } else if (img1.getX() == tx17.getX() && img1.getY() == tx17.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx18.getX());
                    img1.setY(tx18.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx19.getX());
                    img1.setY(tx19.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                }
            } else if (img1.getX() == tx18.getX() && img1.getY() == tx18.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx19.getX());
                    img1.setY(tx19.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                }
            } else if (img1.getX() == tx19.getX() && img1.getY() == tx19.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx20.getX());
                    img1.setY(tx20.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                }
            } else if (img1.getX() == tx20.getX() && img1.getY() == tx20.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx21.getX());
                    img1.setY(tx21.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                }
            } else if (img1.getX() == tx21.getX() && img1.getY() == tx21.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx22.getX());
                    img1.setY(tx22.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx27.getX());
                    img1.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx6.getX());
                            img1.setY(tx6.getY());
                        }
                    }, 1000);
                }
            } else if (img1.getX() == tx22.getX() && img1.getY() == tx22.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx23.getX());
                    img1.setY(tx23.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx27.getX());
                    img1.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx6.getX());
                            img1.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx28.getX());
                    img1.setY(tx28.getY());
                }
            } else if (img1.getX() == tx23.getX() && img1.getY() == tx23.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx24.getX());
                    img1.setY(tx24.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx27.getX());
                    img1.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx6.getX());
                            img1.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx28.getX());
                    img1.setY(tx28.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx29.getX());
                    img1.setY(tx29.getY());
                }
            } else if (img1.getX() == tx24.getX() && img1.getY() == tx24.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx25.getX());
                    img1.setY(tx25.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx27.getX());
                    img1.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx6.getX());
                            img1.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx28.getX());
                    img1.setY(tx28.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx29.getX());
                    img1.setY(tx29.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx30.getX());
                    img1.setY(tx30.getY());
                }
            } else if (img1.getX() == tx25.getX() && img1.getY() == tx25.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx26.getX());
                    img1.setY(tx26.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "YEEEAAH !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx45.getX());
                            img1.setY(tx45.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx27.getX());
                    img1.setY(tx27.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx6.getX());
                            img1.setY(tx6.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx28.getX());
                    img1.setY(tx28.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx29.getX());
                    img1.setY(tx29.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx30.getX());
                    img1.setY(tx30.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx31.getX());
                    img1.setY(tx31.getY());
                }
            } else if (img1.getX() == tx28.getX() && img1.getY() == tx28.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx29.getX());
                    img1.setY(tx29.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx30.getX());
                    img1.setY(tx30.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx31.getX());
                    img1.setY(tx31.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx32.getX());
                    img1.setY(tx32.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx33.getX());
                    img1.setY(tx33.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                }
            } else if (img1.getX() == tx29.getX() && img1.getY() == tx29.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx30.getX());
                    img1.setY(tx30.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx31.getX());
                    img1.setY(tx31.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx32.getX());
                    img1.setY(tx32.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx33.getX());
                    img1.setY(tx33.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                }
            } else if (img1.getX() == tx30.getX() && img1.getY() == tx30.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx31.getX());
                    img1.setY(tx31.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx32.getX());
                    img1.setY(tx32.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx33.getX());
                    img1.setY(tx33.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                }
            } else if (img1.getX() == tx31.getX() && img1.getY() == tx31.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx32.getX());
                    img1.setY(tx32.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx33.getX());
                    img1.setY(tx33.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                }
            } else if (img1.getX() == tx32.getX() && img1.getY() == tx32.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx33.getX());
                    img1.setY(tx33.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                }
            } else if (img1.getX() == tx33.getX() && img1.getY() == tx33.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx34.getX());
                    img1.setY(tx34.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                }
            } else if (img1.getX() == tx34.getX() && img1.getY() == tx34.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx35.getX());
                    img1.setY(tx35.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                }
            } else if (img1.getX() == tx35.getX() && img1.getY() == tx35.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx36.getX());
                    img1.setY(tx36.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                }
            } else if (img1.getX() == tx36.getX() && img1.getY() == tx36.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx37.getX());
                    img1.setY(tx37.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx42.getX());
                    img1.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                }
            } else if (img1.getX() == tx37.getX() && img1.getY() == tx37.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx38.getX());
                    img1.setY(tx38.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx42.getX());
                    img1.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx43.getX());
                    img1.setY(tx43.getY());
                }
            } else if (img1.getX() == tx38.getX() && img1.getY() == tx38.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx39.getX());
                    img1.setY(tx39.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx42.getX());
                    img1.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx43.getX());
                    img1.setY(tx43.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx44.getX());
                    img1.setY(tx44.getY());
                }
            } else if (img1.getX() == tx39.getX() && img1.getY() == tx39.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx40.getX());
                    img1.setY(tx40.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx42.getX());
                    img1.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx43.getX());
                    img1.setY(tx43.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx44.getX());
                    img1.setY(tx44.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                }
            } else if (img1.getX() == tx40.getX() && img1.getY() == tx40.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx41.getX());
                    img1.setY(tx41.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx22.getX());
                            img1.setY(tx22.getY());
                        }
                    }, 1000);
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx42.getX());
                    img1.setY(tx42.getY());
                    rolledNumber = rolledNumber * 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx43.getX());
                    img1.setY(tx43.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx44.getX());
                    img1.setY(tx44.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                }
            } else if (img1.getX() == tx42.getX() && img1.getY() == tx42.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx43.getX());
                    img1.setY(tx43.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx44.getX());
                    img1.setY(tx44.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                }
            } else if (img1.getX() == tx43.getX() && img1.getY() == tx43.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx44.getX());
                    img1.setY(tx44.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                }
            } else if (img1.getX() == tx44.getX() && img1.getY() == tx44.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx50.getX());
                    img1.setY(tx50.getY());
                    winner = true;
                }
            } else if (img1.getX() == tx45.getX() && img1.getY() == tx45.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx50.getX());
                    img1.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx45.getX());
                    img1.setY(tx45.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img1.getX() == tx46.getX() && img1.getY() == tx46.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx50.getX());
                    img1.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx46.getX());
                    img1.setY(tx46.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img1.getX() == tx47.getX() && img1.getY() == tx47.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx50.getX());
                    img1.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx47.getX());
                    img1.setY(tx47.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            } else if (img1.getX() == tx48.getX() && img1.getY() == tx48.getY()) {
                if (rolledNumber == 1) {
                    dice.setImageResource(R.drawable.dice1);
                    img1.setX(tx49.getX());
                    img1.setY(tx49.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "SNAKEEEE !!!", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            img1.setX(tx11.getX());
                            img1.setY(tx11.getY());
                        }
                    }, 1000);
                    rolledNumber = rolledNumber / 2;
                } else if (rolledNumber == 2) {
                    dice.setImageResource(R.drawable.dice2);
                    img1.setX(tx50.getX());
                    img1.setY(tx50.getY());
                    winner = true;
                } else if (rolledNumber == 3) {
                    dice.setImageResource(R.drawable.dice3);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 4) {
                    dice.setImageResource(R.drawable.dice4);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 5) {
                    dice.setImageResource(R.drawable.dice5);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                } else if (rolledNumber == 6) {
                    dice.setImageResource(R.drawable.dice6);
                    img1.setX(tx48.getX());
                    img1.setY(tx48.getY());
                    Toast.makeText(Game_1_vs_Com_Normal.this, "LOL !!! TRY AGAIN !!!", Toast.LENGTH_SHORT).show();
                }
            }
            if (winner) {
                roll.setEnabled(false);
                turns.setText("Winner is" + com.getText());
                Intent intent = new Intent(Game_1_vs_Com_Normal.this, Loser.class);
                intent.putExtra("game", "Game_1_vs_Com_Normal");
                startActivity(intent);
                Game_1_vs_Com_Normal.this.finish();
            } else {
                turns.setText(com.getText());
                turn++;
            }
            pts2 = pts2 + rolledNumber;
            points2.setText(String.valueOf(pts2));
        }
    }
}
