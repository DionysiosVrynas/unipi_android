package com.example_calculator2.dennis.unipi_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.SettingInjectorService;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    RadioGroup radiogroup1, radiogroup2, radiogroup3;
    RadioButton rdcolor, rdmusic, rdcolor2;
    String a, b, c, d, e;
    EditText editText, editeText2;
    MediaPlayer ourSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        radiogroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        radiogroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        radiogroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        editText = (EditText) findViewById(R.id.etplayer1);
        editeText2 = (EditText) findViewById(R.id.edplayer2);
        Button save = (Button)findViewById(R.id.btnSAVE);
        ImageView imageView = (ImageView)findViewById(R.id.ivBackVectorSettings);

        ourSong = MediaPlayer.create(Settings.this, R.raw.music);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp1 = getSharedPreferences("user_SettingsColor", Context.MODE_PRIVATE);

                ID1();
                SharedPreferences.Editor editor1 = sp1.edit();
                editor1.putString("color", a);
                editor1.apply();

                ID2();
                switch (b) {
                    case "ON":
                        launchMusic();
                        break;
                    case "OFF":
                        stopMusic();
                        break;
                    default:
                        stopMusic();
                        break;
                }

                editor1.putString("music", b);
                editor1.apply();

                ID3();
                editor1.putString("color2", c);
                editor1.apply();

                ID4();
                editor1.putString("editText", d);
                editor1.apply();

                ID5();
                editor1.putString("editText2", e);
                editor1.apply();

                Toast.makeText(Settings.this, "Saved!!!", Toast.LENGTH_SHORT).show();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, Snake.class);
                startActivity(intent);
                Settings.this.finish();
            }
        });
    }

    public String ID1() {
        int id1 = radiogroup1.getCheckedRadioButtonId();
        rdcolor = (RadioButton) radiogroup1.findViewById(id1);
        a = rdcolor.getText().toString();
        return a;
    }

    public String ID2() {
        int id2 = radiogroup3.getCheckedRadioButtonId();
        rdmusic = (RadioButton) radiogroup3.findViewById(id2);
        b = rdmusic.getText().toString();
        return b;
    }

    public String ID3() {
        int id3 = radiogroup2.getCheckedRadioButtonId();
        rdcolor2 = (RadioButton) radiogroup2.findViewById(id3);
        c = rdcolor2.getText().toString();
        return c;
    }

    public String ID4() {
        d = editText.getText().toString();
        return d;
    }

    public String ID5() {
        e = editeText2.getText().toString();
        return e;
    }

    public void launchMusic() {
        ourSong.setLooping(true);
        ourSong.start();
    }

    public void stopMusic() {
        ourSong.stop();
        ourSong.release();
    }
}
