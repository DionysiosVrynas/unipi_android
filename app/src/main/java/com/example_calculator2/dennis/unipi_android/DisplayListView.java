package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DisplayListView extends AppCompatActivity {

    String Json_string;
    JSONObject jsonObject;
    JSONArray jsonArray;
    PlayerAdapter playerAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_list_view);

        ImageView imageView = (ImageView)findViewById(R.id.imageView12);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayListView.this, Highscore.class);
                startActivity(intent);
            }
        });

        //Receive Json
        Json_string = getIntent().getExtras().getString("Json_data");
        listView = (ListView)findViewById(R.id.listview);
        playerAdapter = new PlayerAdapter(this, R.layout.activity_row_layout);
        listView.setAdapter(playerAdapter);
        try {
            // Transform json to json object
            jsonObject = new JSONObject(Json_string);
            // Create array with name highscore
            jsonArray = jsonObject.getJSONArray("highscores");
            int count =0 ;
            String id,name,highscore;
            while ( count < jsonArray.length()){
                // Put id , name and highscore into array
                JSONObject JO = jsonArray.getJSONObject(count);
                id = JO.getString("id");
                name = JO.getString("name");
                highscore = JO.getString("highscore");
                Players players = new Players(id,name,highscore);
                playerAdapter.add(players);
                count++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

