package com.example_calculator2.dennis.unipi_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Highscore extends AppCompatActivity {

    String Json,Json1, iduser;
    ImageView imageView, myimage, firstplayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        Button getJSON = (Button) findViewById(R.id.btnJSON);
        Button parseJson = (Button) findViewById(R.id.btnPARSE);
        imageView = (ImageView) findViewById(R.id.imBackVectorHighscore);
        myimage = (ImageView) findViewById(R.id.my_image);
        firstplayer = (ImageView) findViewById(R.id.firstplayerimage);

        // Receiave data from welcome.class
        SharedPreferences sp2 = getSharedPreferences("user_id", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor2 = sp2.edit();
        iduser = sp2.getString("user_id",iduser);
        editor2.apply();

        // show The Image in a ImageView
        new DownloadImageTask(myimage)
                .execute("http://83.212.102.242:80/uploads/" + iduser + ".jpg");

        // show The Image in a ImageView
        new BackgroundWorker2().execute();


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Highscore.this, Snake.class);
                startActivity(intent);
                Highscore.this.finish();
            }
        });

        getJSON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BackgroundTask().execute();
            }
        });

        parseJson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Json == null) {
                    Toast.makeText(getApplicationContext(), "Failllleeeeddddd", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(Highscore.this, DisplayListView.class);
                    intent.putExtra("Json_data", Json);
                    startActivity(intent);
                    Highscore.this.finish();
                }
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        String json_url;

        @Override
        protected void onPreExecute() {
            json_url = "http://83.212.102.242/unipi_android_get.php";
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                while ((Json = bufferedReader.readLine()) != null) {
                    stringBuilder.append(Json).append("\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            TextView textview = (TextView) findViewById(R.id.txHIGHSCORE);
            textview.setText(result);
            Json = result;
        }
    }

    private class BackgroundWorker2 extends AsyncTask<String, Void, String> {
        private String result;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String login_url = "http://83.212.102.242:80/unipi_android_get_unique_id.php";
            try {
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            result = result.trim();

            new DownloadImageTask(firstplayer)
                    .execute("http://83.212.102.242:80/uploads/" + result + ".jpg");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}

