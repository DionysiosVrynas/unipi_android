package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Loser extends AppCompatActivity {

    private  Button retry,cancel;
    private String game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loser);

        retry = (Button)findViewById(R.id.buttonretry_lose);
        cancel = (Button)findViewById(R.id.buttoncancel_lose);

        game = getIntent().getExtras().getString("game");

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (game) {
                    case "Game_1_vs_Com_HARD": {
                        Intent intent = new Intent(Loser.this, Game_1_vs_Com_HARD.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                    case "Game_1_vs_Com_Normal": {
                        Intent intent = new Intent(Loser.this, Game_1_vs_Com_Normal.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                    case "Game_1_vs_Com_EASY": {
                        Intent intent = new Intent(Loser.this, Game_1_vs_Com_EASY.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                    case "Game_Multi_NORMAL": {
                        Intent intent = new Intent(Loser.this, Game_Multi_NORMAL.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                    case "Game_Multi_EASY": {
                        Intent intent = new Intent(Loser.this, Game_Multi_EASY.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                    case "Game_Multi_HARD": {
                        Intent intent = new Intent(Loser.this, Game_Multi_HARD.class);
                        startActivity(intent);
                        Loser.this.finish();
                        break;
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Loser.this, Snake.class);
                startActivity(intent);
                Loser.this.finish();
            }
        });
    }
}
