package com.example_calculator2.dennis.unipi_android;


class Players {

    Players(String id, String name, String highscore){
        this.setId(id);
        this.setName(name);
        this.setHighscore(highscore);
    }

    private String id,name,highscore;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHighscore() {
        return highscore;
    }

    public void setHighscore(String highscore) {
        this.highscore = highscore;
    }
}
