package com.example_calculator2.dennis.unipi_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example_calculator2.dennis.unipi_android.model.Student;
import com.example_calculator2.dennis.unipi_android.service.APIService;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class Welcome extends AppCompatActivity {

    LoginButton loginButton;
    CallbackManager callbackManager;
    TextView showresult;
    EditText editText, editText2;
    Button buttonlogin;
    SharedPreferences sp2;
    SharedPreferences.Editor editor2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(Welcome.this);
        setContentView(R.layout.activity_welcome);

        // We want to save user_id to call it from game_screen
        sp2 = getSharedPreferences("user_id", Context.MODE_PRIVATE);
        editor2 = sp2.edit();

        editText = (EditText) findViewById(R.id.editText3);
        editText2 = (EditText) findViewById(R.id.editText4);
        buttonlogin = (Button) findViewById(R.id.btnLogin);
        showresult = (TextView) findViewById(R.id.textView62);

        // We create loginbtn for Facebook
        loginButton = (LoginButton) findViewById(R.id.fb_login_btn);
        callbackManager = CallbackManager.Factory.create();
        //Login with facebook
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(Welcome.this, "Login Success \n" + loginResult.getAccessToken(), Toast.LENGTH_SHORT).show();
                        showresult.setText(loginResult.getAccessToken().getUserId());
                        editor2.putString("user_id", showresult.getText().toString());
                        editor2.apply();

                        new BackgroundWorker1().execute(showresult.getText().toString());
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(Welcome.this, "Login canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(Welcome.this, "error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        // We create registerbtn
        Button btnregister = (Button) findViewById(R.id.btnregistration);
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Welcome.this, Registration.class);
                startActivity(intent);
                Welcome.this.finish();
            }
        });

        // We create loginbtn for Mysql
        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Call function with parameters id and password
                new BackgroundWorker().execute(editText2.getText().toString(), editText.getText().toString());
            }
        });

        Button exit = (Button)findViewById(R.id.btnExit_from_game);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Welcome.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private class BackgroundWorker extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialog;
        private String result;

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(Welcome.this).create();
            alertDialog.setTitle("Login Status");
        }

        @Override
        protected String doInBackground(String... params) {
            String login_url = "http://83.212.102.242/user_login.php";
            try {
                String id = params[0];
                String password = params[1];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8") + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            alertDialog.setMessage(result);
            alertDialog.show();
            if (result.equalsIgnoreCase(" Login success")) {
                editor2.putString("user_id", editText2.getText().toString());
                editor2.apply();
                Intent intent = new Intent(Welcome.this, Snake.class);
                startActivity(intent);
                Welcome.this.finish();
            } else if (result.equalsIgnoreCase(" Login failed")) {
                Toast.makeText(Welcome.this, "Wrong id or password", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Welcome.this, "Try again later!!!! Connectivity error", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class BackgroundWorker1 extends AsyncTask<String, Void, String> {
        private String result;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String login_url = "http://83.212.102.242:80/unipi_android_get_highscore.php";
            try {
                String id = params[0];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase(" 0 results ")) {
                Retrofit retrofit1 = new Retrofit.Builder()
                        .baseUrl("http://83.212.102.242/unipi_android_post.php")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                APIService service1 = retrofit1.create(APIService.class);
                Student student1 = new Student();
                student1.setName(showresult.getText().toString());
                student1.setId(showresult.getText().toString());
                Call<Student> call1 = service1.insertStudentInfo(student1.getId(),student1.getName(),"0");
                call1.enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Response<Student> response, Retrofit retrofit) {
                        Log.d("onResponse", "" + response.code() +
                                "response body " + response.body() + "response error " + response.errorBody() + " responseMessage " + response.message());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("onFailure", t.toString());
                    }
                });
                Intent intent = new Intent(Welcome.this, Snake.class);
                startActivity(intent);
                Welcome.this.finish();
            } else {
                Intent intent = new Intent(Welcome.this, Snake.class);
                startActivity(intent);
                Welcome.this.finish();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}