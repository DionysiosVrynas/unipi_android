package com.example_calculator2.dennis.unipi_android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example_calculator2.dennis.unipi_android.model.Student;
import com.example_calculator2.dennis.unipi_android.service.APIService;

import java.io.ByteArrayOutputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class Winner extends AppCompatActivity {

    private String game;
    private ImageView captureimage;
    private static final int CAMERA_REQUEST = 1;
    String winner_name;
    String winner_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        winner_name = getIntent().getExtras().getString("winnre's_name");
        winner_id = getIntent().getExtras().getString("winnersID");
        game = getIntent().getExtras().getString("game");
        captureimage = (ImageView) findViewById(R.id.imageViewwinner);
        Button retry = (Button) findViewById(R.id.buttonpplayagain);
        Button cancel = (Button) findViewById(R.id.buttoncancel);
        TextView textView = (TextView) findViewById(R.id.textViewWinner);
        textView.setText(winner_name);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Winner.this, Snake.class);
                startActivity(intent);
                Winner.this.finish();
            }
        });

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (game) {
                    case "Game_1_vs_Com_HARD": {
                        Intent intent = new Intent(Winner.this, Game_1_vs_Com_HARD.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                    case "Game_1_vs_Com_Normal": {
                        Intent intent = new Intent(Winner.this, Game_1_vs_Com_Normal.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                    case "Game_1_vs_Com_EASY": {
                        Intent intent = new Intent(Winner.this, Game_1_vs_Com_EASY.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                    case "Game_Multi_NORMAL": {
                        Intent intent = new Intent(Winner.this, Game_Multi_NORMAL.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                    case "Game_Multi_EASY": {
                        Intent intent = new Intent(Winner.this, Game_Multi_EASY.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                    case "Game_Multi_HARD": {
                        Intent intent = new Intent(Winner.this, Game_Multi_HARD.class);
                        startActivity(intent);
                        Winner.this.finish();
                        break;
                    }
                }
            }
        });
    }

    public void launchCamera(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            Bitmap bitmap1 = (Bitmap) bundle.get("data");
            captureimage.setImageBitmap(bitmap1);
            uploadimage(bitmap1);
        }
    }

    public void uploadimage(Bitmap bitmap1){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://83.212.102.242:80/unipi_android_image_post.php")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Create a service
        APIService service = retrofit.create(APIService.class);

        String photoTostring = imageToString(bitmap1);

        Call<Student> call = service.insertimage(winner_id,winner_name, photoTostring);

        call.enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Response<Student> response, Retrofit retrofit) {
                Log.d("onResponse", "" + response.code() +
                        "response body " + response.body() + "response error " + response.errorBody() + " responseMessage " + response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes,Base64.DEFAULT);
    }
}
